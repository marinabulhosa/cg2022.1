
#include "Image.h"

float F(float x, float y){
	return x - 3*y + 200;
}

int main(){
	Image img(600, 600);
	img.fill(white);
	for(int y = 0; y < img.height(); y++)
		for(int x = 0; x < img.width(); x++)
			if(F(x,y) <= 0)
				img(x,y) = red;
		

	img.savePNG("output.png");
}
